Meteor.startup(function () {
  if (Items.find().count() === 0) {
  	Items.insert({
  		name: 'Capple', 
  		code: 'a'
  	});

  	Items.insert({
  		name: 'Eclair', 
  		code: 'b'
  	});

  	Items.insert({
  		name: 'Erza Scarlet', 
  		code: 'c'
  	});

  	Items.insert({
  		name: 'Froyo', 
  		code: 'd'
  	});

  	Items.insert({
  		name: 'Gingerbread', 
  		code: 'e'
  	});

  	Items.insert({
  		name: 'Honeycomb', 
  		code: 'f'
  	});

  	Items.insert({
  		name: 'Ice Cream', 
  		code: 'g'
  	});

  	Items.insert({
  		name: 'Jelly Bean', 
  		code: 'h'
  	});

  	Items.insert({
  		name: 'Kit Kat', 
  		code: 'i'
  	});

  	Items.insert({
  		name: 'Lollipop', 
  		code: 'j'
  	});
  }
});